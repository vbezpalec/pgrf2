package control;

import geometryObjects.*;
import geometryObjects.SimpleBicubic;
import model.Solid;
import raster.ImageBuffer;
import raster.ZbufferVisibility;
import render.Rasterizer;
import render.Renderer;
import transforms.*;
import view.Panel;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

public class Controller3D implements Controller {

    private final Panel panel;

    private int width, height;
    private int ox, oy;
    private ZbufferVisibility zbufferVisibility;
    private Rasterizer rasterizer;
    private Renderer renderer;
    private List<Solid> scene = new ArrayList<>();
    private int selectedSolidIndex;
    private Solid selectedSolid;

    public Controller3D(Panel panel) {
        this.panel = panel;
        initObjects(panel.getRaster());
        initListeners(panel);
        redraw();
    }

    public void initObjects(ImageBuffer raster) {
        raster.setClearValue(new Col(0x101010));
        setDimensions();
        zbufferVisibility = new ZbufferVisibility(raster);
        rasterizer = new Rasterizer(zbufferVisibility);
        renderer = new Renderer(rasterizer);
        setPerspectiveProjection();

        scene.add(new ArrowX());
        scene.add(new SimpleLine());
        scene.add(new SimpleCube());
        scene.add(new SimpleTriangle());
        scene.add(new Axis());
        scene.add(new SimpleBicubic());

        selectedSolid = scene.get(0);
    }

    private void setPerspectiveProjection() {
        renderer.setProjection(new Mat4PerspRH(
                Math.PI / 3,
                height / (float) width,
                1,
                50
        ));
    }

    private void setOrthoProjection() {
        renderer.setProjection(new Mat4OrthoRH(
                width / 180f, height / 180f, 1, 200
        ));
    }

    @Override
    public void initListeners(Panel panel) {
        panel.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent ev) {
                if (ev.getButton() == MouseEvent.BUTTON1) {
                    ox = ev.getX();
                    oy = ev.getY();
                    redraw();
                }
            }

            public void mouseReleased(MouseEvent ev) {
                if (ev.getButton() == MouseEvent.BUTTON1) {
                    redraw();
                }
            }
        });

        panel.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent ev) {
                // camera movement
                double azimuthDiff = (ox - ev.getX()) / 5000.0;
                double zenithDiff = (oy - ev.getY()) / 5000.0;
                renderer.moveCamera(azimuthDiff, zenithDiff);
                redraw();

            }
        });

        panel.addMouseWheelListener(new MouseWheelListener() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
//                mousewheel zoom
                int d = e.getWheelRotation();

                if (d < 0) {
                    renderer.setCamera(renderer.getCamera().forward(0.5));
                } else if (d > 0) {
                    renderer.setCamera(renderer.getCamera().backward(0.5));
                }
                redraw();
            }
        });

        panel.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent key) {
                // out.print(key.getKeyCode());
                switch (key.getKeyCode()) {
                    case KeyEvent.VK_BACK_SPACE:
                        hardClear();
                        break;
                }
                switch (key.getKeyChar()) {
                    case 'a':
                        renderer.cameraLeft(0.5);
                        break;
                    case 'w':
                        renderer.cameraForward(0.5);
                        break;
                    case 's':
                        renderer.cameraBackward(0.5);
                        break;
                    case 'd':
                        renderer.cameraRight(0.5);
                        break;
                    case '+':
                        renderer.cameraUp(0.5);
                        break;
                    case '-':
                        renderer.cameraDown(0.5);
                        break;
                    case 'o':
                        setOrthoProjection();
                        break;
                    case 'p':
                        setPerspectiveProjection();
                        break;
                    case 'j':
                        renderer.setWireframeOnly(!renderer.getWireframeOnly());
                        break;
                    case 'b':
                        if (selectedSolidIndex == scene.size() - 1) {
                            selectedSolidIndex = 0;
                        } else {
                            selectedSolidIndex++;
                        }
                        selectedSolid = scene.get(selectedSolidIndex);
                        break;
                    case 'u':
                        selectedSolid.setModel(selectedSolid.getModel().mul(new Mat4Scale(0.9)));
                        break;
                    case 'U':
                        selectedSolid.setModel(selectedSolid.getModel().mul(new Mat4Scale(1.1)));
                        break;
                    case 'r':
                        selectedSolid.setModel(selectedSolid.getModel().mul(new Mat4RotX(0.2)));
                        break;
                    case 'R':
                        selectedSolid.setModel(selectedSolid.getModel().mul(new Mat4RotY(0.2)));
                        break;
                    case 't':
                        selectedSolid.setModel(selectedSolid.getModel().mul(new Mat4RotZ(0.2)));
                        break;
                    case '1':
                        selectedSolid.setModel(selectedSolid.getModel().mul(new Mat4Transl(0.2, 0, 0)));
                        break;
                    case '2':
                        selectedSolid.setModel(selectedSolid.getModel().mul(new Mat4Transl(0, 0.2, 0)));
                        break;
                    case '3':
                        selectedSolid.setModel(selectedSolid.getModel().mul(new Mat4Transl(0, 0, 0.2)));
                        break;
                }
                redraw();
            }
        });

        panel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                panel.resize();
                initObjects(panel.getRaster());
                redraw();
            }
        });
    }

    private void redraw() {
        panel.clear();
        zbufferVisibility.clear();

        setDimensions();
        Graphics g = panel.getRaster().getGraphics();
        g.setColor(Color.white);


        renderer.render(scene);

        panel.repaint();
    }

    private void setDimensions() {
        width = panel.getRaster().getWidth();
        height = panel.getRaster().getHeight();
    }

    private void hardClear() {
        panel.clear();
        initObjects(panel.getRaster());
    }

}
