package geometryObjects;

import model.Part;
import model.Solid;
import model.TypeTopology;
import model.Vertex;
import transforms.Mat4Transl;
import transforms.Point3D;

public class SimpleCube extends Solid {

    public SimpleCube() {
        Vertex a = new Vertex(new Point3D(-1, -1, -1), getRandomColor());
        Vertex b = new Vertex(new Point3D(1, -1, -1), getRandomColor());
        Vertex c = new Vertex(new Point3D(1, 1, -1), getRandomColor());
        Vertex d = new Vertex(new Point3D(-1, 1, -1), getRandomColor());

        Vertex e = new Vertex(new Point3D(-1, -1, 1), getRandomColor());
        Vertex f = new Vertex(new Point3D(1, -1, 1), getRandomColor());
        Vertex g = new Vertex(new Point3D(1, 1, 1), getRandomColor());
        Vertex h = new Vertex(new Point3D(-1, 1, 1), getRandomColor());

        getVertices().add(a);
        getVertices().add(b);
        getVertices().add(c);
        getVertices().add(d);
        getVertices().add(e);
        getVertices().add(f);
        getVertices().add(g);
        getVertices().add(h);

        getIndices().add(0);
        getIndices().add(1);
        getIndices().add(2);
        getIndices().add(0);
        getIndices().add(3);
        getIndices().add(2);

        getIndices().add(0);
        getIndices().add(4);
        getIndices().add(5);
        getIndices().add(0);
        getIndices().add(1);
        getIndices().add(5);

        getIndices().add(1);
        getIndices().add(5);
        getIndices().add(6);
        getIndices().add(1);
        getIndices().add(2);
        getIndices().add(6);

        getIndices().add(4);
        getIndices().add(7);
        getIndices().add(3);
        getIndices().add(4);
        getIndices().add(0);
        getIndices().add(3);

        getIndices().add(7);
        getIndices().add(3);
        getIndices().add(2);
        getIndices().add(7);
        getIndices().add(6);
        getIndices().add(2);

        getIndices().add(7);
        getIndices().add(4);
        getIndices().add(5);
        getIndices().add(7);
        getIndices().add(6);
        getIndices().add(5);

        getParts().add(new Part(TypeTopology.TRIANGLES, 0, 12));

        setModel(new Mat4Transl(1, 1., 4.));
    }

}
