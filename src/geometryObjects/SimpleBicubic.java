package geometryObjects;

import model.Part;
import model.Solid;
import model.TypeTopology;
import model.Vertex;
import transforms.*;

public class SimpleBicubic extends Solid {
    private final Point3D[] points;
    int rowLength = 16;

    public SimpleBicubic() {
        points = new Point3D[16];
        points[0] = (new Point3D(1, 1, -1));
        points[1] = (new Point3D(-1, 8, -1));
        points[2] = (new Point3D(1, 1.5, 2));
        points[3] = (new Point3D(-1, 3, 4));
        points[4] = (new Point3D(1, 1, -1));
        points[5] = (new Point3D(-1, 8, 2));
        points[6] = (new Point3D(1, 1, 2));
        points[7] = (new Point3D(-2, 3, 3));
        points[8] = (new Point3D(1, 2, 1));
        points[9] = (new Point3D(-1, 8, -1));
        points[10] = (new Point3D(3, 1, 2));
        points[11] = (new Point3D(-1, 4, 1));
        points[12] = (new Point3D(1.9, 1, -4));
        points[13] = (new Point3D(-1, 8, -1));
        points[14] = (new Point3D(9, 1, 2.4));
        points[15] = (new Point3D(-1, 3, 1));
        make();
        setModel(new Mat4Transl(0, 0, 2));
    }

    private void make() {
        Bicubic shape = new Bicubic(Cubic.FERGUSON, points);

        double increment = 1. / (rowLength - 1); // 256 in total
        for (double u = 0; u <= 1; u += increment) {
            for (double v = 0; v <= 1; v += increment) {
                Point3D result = shape.compute(u, v);
                getVertices().add(new Vertex(result, getRandomColor()));
            }
        }

        createFilledPlane();
    }

    private void createMesh() {
        int a = 0;
        for (int i = 0; i < getVertices().size(); i++) {
            if (!((i + 1) % rowLength == 0)) {
                getIndices().add(i);
                getIndices().add(i + 1);
                getParts().add(new Part(TypeTopology.LINES, a, 1));
                a += 2;
            }
            if ((i + rowLength) < getVertices().size()) {
                getIndices().add(i);
                getIndices().add(i + 16);
                getParts().add(new Part(TypeTopology.LINES, a, 1));
                a += 2;
            }
        }
    }

    private void createFilledPlane() {
        int a = 0;
        for (int i = 0; i < getVertices().size(); i++) {
            if (!((i + 1) % rowLength == 0) && (i + rowLength) < getVertices().size()) {
                getIndices().add(i);
                getIndices().add(i + 1);
                getIndices().add(i + 16);
                getParts().add(new Part(TypeTopology.TRIANGLES, a, 1));
                a += 3;
                getIndices().add(i + 1);
                getIndices().add(i + 16);
                getIndices().add(i + 16 + 1);
                getParts().add(new Part(TypeTopology.TRIANGLES, a, 1));
                a += 3;
            }
        }
    }
}
