package geometryObjects;

import model.Part;
import model.Solid;
import model.TypeTopology;
import model.Vertex;
import transforms.Col;
import transforms.Mat4Scale;
import transforms.Mat4Transl;
import transforms.Point3D;

public class SimpleTriangle extends Solid {
    public SimpleTriangle() {
        Vertex a = new Vertex(new Point3D(1, 1, 0), new Col(1., 0., 0.));
        Vertex b = new Vertex(new Point3D(-1, 0, 0), new Col(0., 1.0, 0.));
        Vertex c = new Vertex(new Point3D(0, -1, 0), new Col(0., 0., 1.));

        getVertices().add(a);
        getVertices().add(b);
        getVertices().add(c);

        getIndices().add(0);
        getIndices().add(1);
        getIndices().add(2);

        getParts().add(new Part(TypeTopology.TRIANGLES, 0, 1));


        setModel(new Mat4Transl(-1, 0., 0.).mul(new Mat4Scale(2.)));
    }
}
