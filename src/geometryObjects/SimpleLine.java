package geometryObjects;

import model.Part;
import model.Solid;
import model.TypeTopology;
import model.Vertex;
import transforms.Col;
import transforms.Point3D;

public class SimpleLine extends Solid {

    public SimpleLine() {
        super();
        Vertex a = new Vertex(new Point3D(0, 0, 0), new Col(0x00ff00));
        Vertex b = new Vertex(new Point3D(1, 1, 0), new Col(0xff0000));

        getVertices().add(a);
        getVertices().add(b);

        getIndices().add(0);
        getIndices().add(1);

        getParts().add(new Part(TypeTopology.LINES, 0, 1));

    }

}