package geometryObjects;

import model.Part;
import model.Solid;
import model.TypeTopology;
import model.Vertex;
import transforms.Col;
import transforms.Mat4Identity;
import transforms.Point3D;

import java.util.ArrayList;
import java.util.List;

public class Axis extends Solid {

    public Axis() {
        Vertex x00 = new Vertex(new Point3D(0, 0, 0), new Col(0xff0000));
        Vertex x01 = new Vertex(new Point3D(1, 0, 0), new Col(0xff0000));
        Vertex x11 = new Vertex(new Point3D(0.95, 0.05, 0), new Col(0xff0000));
        Vertex x12 = new Vertex(new Point3D(0.95, 0, 0.05), new Col(0xff0000));
        Vertex x13 = new Vertex(new Point3D(0.95, -0.05, 0), new Col(0xff0000));
        Vertex x14 = new Vertex(new Point3D(0.95, 0, -0.05), new Col(0xff0000));

        Vertex y00 = new Vertex(new Point3D(0, 0, 0), new Col(0x00ff00));
        Vertex y01 = new Vertex(new Point3D(0, 1, 0), new Col(0x00ff00));
        Vertex y11 = new Vertex(new Point3D(0.05, 0.95, 0), new Col(0x00ff00));
        Vertex y12 = new Vertex(new Point3D(0, 0.95, 0.05), new Col(0x00ff00));
        Vertex y13 = new Vertex(new Point3D(-0.05, 0.95, 0), new Col(0x00ff00));
        Vertex y14 = new Vertex(new Point3D(0, 0.95, -0.05), new Col(0x00ff00));

        Vertex z00 = new Vertex(new Point3D(0, 0, 0), new Col(0x0000ff));
        Vertex z01 = new Vertex(new Point3D(0, 0, 1), new Col(0x0000ff));
        Vertex z11 = new Vertex(new Point3D(0.05, 0, 0.95), new Col(0x0000ff));
        Vertex z12 = new Vertex(new Point3D(0, 0.05, 0.95), new Col(0x0000ff));
        Vertex z13 = new Vertex(new Point3D(-0.05, 0, 0.95), new Col(0x0000ff));
        Vertex z14 = new Vertex(new Point3D(0, -0.05, 0.95), new Col(0x0000ff));

        getVertices().add(x00);
        getVertices().add(x01);
        getVertices().add(x11);
        getVertices().add(x12);
        getVertices().add(x13);
        getVertices().add(x14);
        getVertices().add(y00);
        getVertices().add(y01);
        getVertices().add(y11);
        getVertices().add(y12);
        getVertices().add(y13);
        getVertices().add(y14);
        getVertices().add(z00);
        getVertices().add(z01);
        getVertices().add(z11);
        getVertices().add(z12);
        getVertices().add(z13);
        getVertices().add(z14);

        //x
        getIndices().add(0);
        getIndices().add(1);

        getIndices().add(1);
        getIndices().add(2);
        getIndices().add(3);
        getIndices().add(-1);
        getIndices().add(3);
        getIndices().add(4);
        getIndices().add(-1);
        getIndices().add(4);
        getIndices().add(5);
        getIndices().add(-1);
        getIndices().add(5);
        getIndices().add(2);
        //y
        getIndices().add(6);//14
        getIndices().add(7);

        getIndices().add(7);
        getIndices().add(8);
        getIndices().add(9);
        getIndices().add(-1);
        getIndices().add(9);
        getIndices().add(10);
        getIndices().add(-1);
        getIndices().add(10);
        getIndices().add(11);
        getIndices().add(-1);
        getIndices().add(11);
        getIndices().add(8);
        //z
        getIndices().add(12);//28
        getIndices().add(13);

        getIndices().add(13);
        getIndices().add(14);
        getIndices().add(15);
        getIndices().add(-1);
        getIndices().add(15);
        getIndices().add(16);
        getIndices().add(-1);
        getIndices().add(16);
        getIndices().add(17);
        getIndices().add(-1);
        getIndices().add(17);
        getIndices().add(14);

        getParts().add(new Part(TypeTopology.LINES, 0, 1));
        getParts().add(new Part(TypeTopology.TRIANGLE_FAN, 2, 4));
        getParts().add(new Part(TypeTopology.LINES, 14, 1));
        getParts().add(new Part(TypeTopology.TRIANGLE_FAN, 16, 4));
        getParts().add(new Part(TypeTopology.LINES, 28, 1));
        getParts().add(new Part(TypeTopology.TRIANGLE_FAN, 30, 4));

        //  model = new Mat4Identity();
    }

}
