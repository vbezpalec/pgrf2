package geometryObjects;

import model.Part;
import model.Solid;
import model.TypeTopology;
import model.Vertex;
import transforms.Col;
import transforms.Mat4RotZ;
import transforms.Mat4Transl;
import transforms.Point3D;

public class ArrowX extends Solid {
    public ArrowX() {
        super();
        getVertices().add(new Vertex(new Point3D(0, 0, 0)));
        getVertices().add(new Vertex(new Point3D(1, 0, 0)));

        getVertices().add(new Vertex(new Point3D(0.95, 0.1, 0), new Col(0., 0., 1.)));
        getVertices().add(new Vertex(new Point3D(0.95, -0.1, 0), new Col(0., 0., 1.)));
        getVertices().add(new Vertex(new Point3D(0.95, 0, 0.1), new Col(0., 0., 1.)));

        getIndices().add(0);
        getIndices().add(1);

        getIndices().add(1);
        getIndices().add(2);
        getIndices().add(3);

        getIndices().add(1);
        getIndices().add(2);
        getIndices().add(4);

        getIndices().add(1);
        getIndices().add(4);
        getIndices().add(3);

        getIndices().add(4);
        getIndices().add(2);
        getIndices().add(3);

        //edge
        getParts().add(new Part(TypeTopology.LINES, 0, 1));
        //triangle
        getParts().add(new Part(TypeTopology.TRIANGLES, 2, 4));

        setModel(new Mat4Transl(0, 3., 0.).mul(new Mat4RotZ(2.)));
    }
}
