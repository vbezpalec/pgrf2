package model;

import transforms.Col;
import transforms.Mat4;
import transforms.Point3D;

public class Vertex implements Vectorizable<Vertex> {
    private final Point3D position;
    private Col color;
    private double one = 1;

    public Vertex(Point3D position) {
        color = new Col();
        this.position = position;
    }

    public Vertex(Point3D position, Col color) {
        this.position = position;
        this.color = color;
    }

    public Vertex(Point3D position, Col color, double one) {
        this(position, color);
        this.one = one;
    }

    public Point3D getPosition() {
        return position;
    }

    public Col getColor() {
        return color;
    }

    public Vertex dehomog() {
        one = 1 / getW();
        return mul(1 / getW());
    }

    private Vertex setColor(Col color) {
        this.color = color;
        return this;
    }

    @Override
    public Vertex mul(double d) {
        return new Vertex(position.mul(d), color.mul(d));
    }

    public Vertex mul(Mat4 d) {
        return new Vertex(position.mul(d), color, one);
    }

    @Override
    public Vertex add(Vertex vertex) {
        return new Vertex(position.add(vertex.position), color.add(vertex.color), one);
    }

    public Vertex interpolate(Vertex vertex) {
        double t = (0 - getZ()) / (vertex.getZ() - getZ());
        return mul(1 - t).add(vertex.mul(t)).setColor(vertex.getColor());
    }

    @Override
    public String toString() {
        return "Vertex{" + position + '}';
    }

    public double getX() {
        return position.getX();
    }

    public double getY() {
        return position.getY();
    }

    public double getZ() {
        return position.getZ();
    }

    public double getW() {
        return position.getW();
    }

    public double getOne() {
        return one;
    }
}
