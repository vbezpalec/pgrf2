package model;

public class Part {
    private TypeTopology type;
    private int startIdx;
    private int count;

    public Part(TypeTopology type, int startIdx, int count) {
        this.type = type;
        this.startIdx = startIdx;
        this.count = count;
    }

    public TypeTopology getType() {
        return type;
    }

    public int getStartIdx() {
        return startIdx;
    }

    public int getCount() {
        return count;
    }
}
