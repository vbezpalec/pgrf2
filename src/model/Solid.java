package model;

import transforms.Col;
import transforms.Mat4;
import transforms.Mat4Identity;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Solid {
    private List<Vertex> vertices;
    private List<Integer> indices;
    private List<Part> parts;
    private Mat4 model;

    public Solid() {
        vertices = new ArrayList<>();
        indices = new ArrayList<>();
        parts = new ArrayList<>();
        model = new Mat4Identity();
    }

    public List<Vertex> getVertices() {
        return vertices;
    }

    public List<Integer> getIndices() {
        return indices;
    }

    public List<Part> getParts() {
        return parts;
    }

    public Mat4 getModel() {
        return model;
    }

    public void setModel(Mat4 model) {
        this.model = model;
    }

    // generuje nahodou barvu
    public Col getRandomColor() {
        Random random = new Random();
        int r = random.nextInt(255);
        int g = random.nextInt(255);
        int b = random.nextInt(255);
        return new Col(r, g, b);
    }
}
