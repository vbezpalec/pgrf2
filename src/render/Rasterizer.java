package render;

import model.Triangle;
import model.Vertex;
import raster.ZbufferVisibility;
import transforms.Point3D;
import transforms.Vec3D;

import java.awt.*;

import static java.lang.Math.abs;

public class Rasterizer {
    private final ZbufferVisibility zbufferVisibility;
    private final int width, height;

    public Rasterizer(ZbufferVisibility zbufferVisibility) {
        this.zbufferVisibility = zbufferVisibility;
        width = zbufferVisibility.getImage().getWidth();
        height = zbufferVisibility.getImage().getHeight();
    }

    public void rasterizePoint(Vertex a) {
        Vec3D vecA = transformToWindow(a.getPosition().ignoreW());
        zbufferVisibility.drawElementWithZtest((int) vecA.getX(), (int) vecA.getY(), vecA.getZ(), a.getColor());
    }

    public void rasterizeLine(Vertex a, Vertex b, Boolean wireframeOnly) {
        // transform to window
        Vec3D vecA = transformToWindow(a.getPosition().ignoreW());
        Vec3D vecB = transformToWindow(b.getPosition().ignoreW());

        if (wireframeOnly) {
            Graphics g = zbufferVisibility.getImage().getImg().getGraphics();
            g.drawLine((int) vecA.getX(), (int) vecA.getY(), (int) vecB.getX(), (int) vecB.getY());
            return;
        }

        if (abs(vecA.getX() - vecB.getX()) > abs(vecA.getY() - vecB.getY())) {
            if (vecA.getX() > vecB.getX()) {
                Vec3D temp = vecA;
                vecA = vecB;
                vecB = temp;
            }
            for (int x = (int) vecA.getX(); x < vecB.getX(); x++) {
                double t = (x - vecA.getX()) / (vecB.getX() - vecA.getX());
                Vec3D vecAB = vecA.mul(1 - t).add(vecB.mul(t));
                Vertex ab = a.mul(1 - t).add(b.mul(t));
                zbufferVisibility.drawElementWithZtest(x, (int) vecAB.getY(), vecAB.getZ(), ab.getColor());
            }
        } else {
            if (vecA.getY() > vecB.getY()) {
                Vec3D temp = vecA;
                vecA = vecB;
                vecB = temp;
            }
            for (int y = (int) vecA.getY(); y < vecB.getY(); y++) {
                double t = (y - vecA.getY()) / (vecB.getY() - vecA.getY());
                Vertex ab = a.mul(1 - t).add(b.mul(t));
                Vec3D vecAB = vecA.mul(1 - t).add(vecB.mul(t));
                zbufferVisibility.drawElementWithZtest((int) vecAB.getX(), y, vecAB.getZ(), ab.getColor());
            }
        }
    }


    /*
    triangle vertices a,b,c are coordinates in NDC
    x,y is in interval <-1;1>
    z is in interval <0;1>
     */
    public void rasterizeTriangle(Triangle triangle, Boolean wireframeOnly) {

        Vec3D a = transformToWindow(triangle.a.getPosition().ignoreW());
        Vec3D b = transformToWindow(triangle.b.getPosition().ignoreW());
        Vec3D c = transformToWindow(triangle.c.getPosition().ignoreW());

        if (wireframeOnly) {
            Graphics g = zbufferVisibility.getImage().getImg().getGraphics();
            g.drawLine((int) a.getX(), (int) a.getY(), (int) b.getX(), (int) b.getY());
            g.drawLine((int) a.getX(), (int) a.getY(), (int) c.getX(), (int) c.getY());
            g.drawLine((int) c.getX(), (int) c.getY(), (int) b.getX(), (int) b.getY());
            return;
        }

        Vertex vA = triangle.a;
        Vertex vB = triangle.b;
        Vertex vC = triangle.c;

        //sort a,b,c by y coordinate; a.y <= b.y <= c.y
        if (a.getY() > b.getY()) {
            Vertex temp = vA;
            vA = vB;
            vB = temp;
            Vec3D temp2 = a;
            a = b;
            b = temp2;
        }
        if (b.getY() > c.getY()) {
            Vertex temp = vB;
            vB = vC;
            vC = temp;
            Vec3D temp2 = b;
            b = c;
            c = temp2;
        }
        if (a.getY() > b.getY()) {
            Vertex temp = vA;
            vA = vB;
            vB = temp;
            Vec3D temp2 = a;
            a = b;
            b = temp2;
        }

        //set limits for x,y
        for (int y = (int) a.getY(); y < b.getY(); y++) {
            double s1 = (y - a.getY()) / (b.getY() - a.getY());
            Vec3D ab = a.mul(1 - s1).add(b.mul(s1));
            Vertex vAB = vA.mul(1 - s1).add(vB.mul(s1));

            double s2 = (y - a.getY()) / (c.getY() - a.getY());
            Vec3D ac = a.mul(1 - s2).add(c.mul(s2));
            Vertex vAC = vA.mul(1 - s2).add(vC.mul(s2));

            if (ab.getX() > ac.getX()) {
                Vec3D temp = ab;
                ab = ac;
                ac = temp;
                Vertex temp2 = vAB;
                vAB = vAC;
                vAC = temp2;
            }

            for (int x = (int) ab.getX(); x < ac.getX(); x++) {
                //interpolate z coordinate
                //calculate color
                double t = (x - ab.getX()) / (ac.getX() - ab.getX());
                Vec3D abc = ab.mul(1 - t).add(ac.mul(t));
                double z = abc.getZ();
                Vertex vABC = vAB.mul(1 - t).add(vAC.mul(t));
                zbufferVisibility.drawElementWithZtest(x, y, z, vABC.getColor());
            }
        }

        for (int y = (int) b.getY(); y < c.getY(); y++) {
            double s1 = (y - a.getY()) / (c.getY() - a.getY());
            Vec3D ac = a.mul(1 - s1).add(c.mul(s1));
            Vertex vAC = vA.mul(1 - s1).add(vC.mul(s1));

            double s2 = (y - b.getY()) / (c.getY() - b.getY());
            Vec3D bc = b.mul(1 - s2).add(c.mul(s2));
            Vertex vBC = vB.mul(1 - s2).add(vC.mul(s2));

            if (ac.getX() > bc.getX()) {
                Vec3D temp = ac;
                ac = bc;
                bc = temp;
                Vertex temp2 = vAC;
                vAC = vBC;
                vBC = temp2;
            }

            for (int x = (int) ac.getX(); x < bc.getX(); x++) {
                double t = (x - ac.getX()) / (bc.getX() - ac.getX());
                Vec3D abc = ac.mul(1 - t).add(ac.mul(t));
                double z = abc.getZ();
                Vertex vABC = vAC.mul(1 - t).add(vBC.mul(t));
                zbufferVisibility.drawElementWithZtest(x, y, z, vABC.getColor());
            }
        }
    }

    private Vec3D transformToWindow(Vec3D v) {
        return v.mul(new Vec3D(1, -1, 1))
                .add(new Vec3D(1, 1, 0))
                .mul(new Vec3D((width - 1) / 2f, (height - 1) / 2f, 1));
    }

}
