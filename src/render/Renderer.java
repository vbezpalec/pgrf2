package render;

import model.Part;
import model.Solid;
import model.Triangle;
import model.Vertex;
import transforms.*;

import java.util.List;

public class Renderer {

    private final Rasterizer rasterizer;
    private Boolean wireframeOnly;
    private Mat4 projection;

    private Camera camera =
//            new Camera()
//                    .withPosition(new Vec3D(0.5, -6, 2))
//                    .withAzimuth(Math.toRadians(90))
//                    .withZenith(Math.toRadians(-20));
            new Camera(
                    new Vec3D(0, -8, 4),
                    Math.toRadians(90),
                    Math.toRadians(-25),
                    1, true
            );


    public Renderer(Rasterizer rasterizer) {
        this.rasterizer = rasterizer;
        projection = new Mat4Identity();
        wireframeOnly = false;
    }

    public void render(Solid solid) {
        for (Part part : solid.getParts()) {
            int partStartIndex = part.getStartIdx();
            Mat4 model = solid.getModel();
            switch (part.getType()) {
                case LINES:
                    for (int i = 0; i < part.getCount(); i++) {
                        int indexA = partStartIndex + i * 2;
                        int indexB = partStartIndex + i * 2 + 1;
                        Vertex a = solid.getVertices().get(solid.getIndices().get(indexA));
                        Vertex b = solid.getVertices().get(solid.getIndices().get(indexB));
                        a = a.mul(model).mul(camera.getViewMatrix()).mul(projection);
                        b = b.mul(model).mul(camera.getViewMatrix()).mul(projection);
                        clipLine(a, b);
                    }
                    break;
                case TRIANGLES:
                    for (int i = 0; i < part.getCount(); i++) {
                        int indexA = partStartIndex + i * 3;
                        int indexB = partStartIndex + i * 3 + 1;
                        int indexC = partStartIndex + i * 3 + 2;
                        Vertex a = solid.getVertices().get(solid.getIndices().get(indexA));
                        Vertex b = solid.getVertices().get(solid.getIndices().get(indexB));
                        Vertex c = solid.getVertices().get(solid.getIndices().get(indexC));
                        a = a.mul(model).mul(camera.getViewMatrix()).mul(projection);
                        b = b.mul(model).mul(camera.getViewMatrix()).mul(projection);
                        c = c.mul(model).mul(camera.getViewMatrix()).mul(projection);
                        clipTriangle(new Triangle(a, b, c));
                    }
                    break;
                case POINTS:
                    for (int i = 0; i < part.getCount(); i++) {
                        Vertex a = solid.getVertices().get(solid.getIndices().get(i));
                        a = a.mul(model).mul(camera.getViewMatrix()).mul(projection);
                        rasterizer.rasterizePoint(a);
                    }
                    break;
                case TRIANGLE_FAN:
                    for (int i = 0; i < part.getCount(); i++) {
                        int indexB = partStartIndex + i * 3 + 1;
                        int indexC = partStartIndex + i * 3 + 2;
                        Vertex a = solid.getVertices().get(solid.getIndices().get(partStartIndex));
                        Vertex b = solid.getVertices().get(solid.getIndices().get(indexB));
                        Vertex c = solid.getVertices().get(solid.getIndices().get(indexC));
                        a = a.mul(model).mul(camera.getViewMatrix()).mul(projection);
                        b = b.mul(model).mul(camera.getViewMatrix()).mul(projection);
                        c = c.mul(model).mul(camera.getViewMatrix()).mul(projection);
                        clipTriangle(new Triangle(a, b, c));
                    }
                    break;
            }
        }
    }

    public void render(List<Solid> scene) {
        for (Solid s : scene) {
            render(s);
        }
    }

    private void clipLine(Vertex a, Vertex b) {

        if ((-a.getW() > a.getX() && -b.getW() > b.getX()) ||
                (a.getX() > a.getW() && b.getX() > b.getW()) ||
                (-a.getW() > a.getY() && -b.getW() > b.getY()) ||
                (a.getY() > a.getW() && b.getY() > b.getW()) ||
                (a.getZ() > a.getW() && b.getZ() > b.getW()) ||
                (-a.getW() > a.getZ() && -b.getW() > b.getZ())) return;

        if (a.getZ() < b.getZ()) {
            Vertex temp = a;
            a = b;
            b = temp;
        }

        if (a.getZ() < 0) {
            return;
        }

        if (b.getZ() < 0) {
            Vertex ab = a.interpolate(b);
            rasterizer.rasterizeLine(a.dehomog(), ab.dehomog(), wireframeOnly);

        } else {
            rasterizer.rasterizeLine(a.dehomog(), b.dehomog(), wireframeOnly);
        }

    }

    /*
    input: Triangle, vertices have coordinates before dehomogenization
    sort a,b,c by z
    clip by z = 0
    dehomog
    call RasterizerTriangle.rasterize()
    */
    private void clipTriangle(Triangle triangle) {
        Vertex a = triangle.a;
        Vertex b = triangle.b;
        Vertex c = triangle.c;

        if (a.getX() < -a.getW() && b.getX() < -b.getW() && c.getX() < -c.getW() ||
                a.getX() > a.getW() && b.getX() > b.getW() && c.getX() > c.getW() ||
                a.getY() < -a.getW() && b.getY() < -b.getW() && c.getY() < -c.getW() ||
                a.getY() > a.getW() && b.getY() > b.getW() && c.getY() > c.getW() ||
                a.getZ() < 0 && b.getZ() < 0 && c.getZ() < 0 ||
                a.getZ() > a.getW() && b.getZ() > b.getW() && c.getZ() > c.getW()) {
            return;
        }

        // sort by z; a.z > b.z > c.z
        if (a.getZ() < b.getZ()) {
            Vertex temp = a;
            a = b;
            b = temp;
        }
        if (b.getZ() < c.getZ()) {
            Vertex temp = b;
            b = c;
            c = temp;
        }
        if (a.getZ() < b.getZ()) {
            Vertex temp = a;
            a = b;
            b = temp;
        }

        if (a.getZ() < 0) {
            return;
        }

        if (b.getZ() < 0) {
            Vertex ab = a.interpolate(b).dehomog();
            Vertex ac = a.interpolate(c).dehomog();
            rasterizer.rasterizeTriangle(new Triangle(a.dehomog(), ab, ac), wireframeOnly);
        } else if (c.getZ() < 0) {
            Vertex ac = a.interpolate(c).dehomog();
            Vertex bc = b.interpolate(c).dehomog();
            rasterizer.rasterizeTriangle(new Triangle(a.dehomog(), b.dehomog(), ac.dehomog()), wireframeOnly);
            rasterizer.rasterizeTriangle(new Triangle(a.dehomog(), bc, ac), wireframeOnly);
        } else {
            rasterizer.rasterizeTriangle(new Triangle(a.dehomog(), b.dehomog(), c.dehomog()), wireframeOnly);
        }

    }

    public void moveCamera(double azimuthDiff, double zenithDiff) {
        double azimuth = camera.getAzimuth() + azimuthDiff;
        double zenith = camera.getZenith() + zenithDiff;

        if (zenith > Math.PI / 2)
            zenith = Math.PI / 2;
        if (zenith < -Math.PI / 2)
            zenith = -Math.PI / 2;

        camera = camera.withAzimuth(azimuth);
        camera = camera.withZenith(zenith);
    }

    public void setProjection(Mat4 projection) {
        this.projection = projection;
    }

    public Boolean getWireframeOnly() {
        return wireframeOnly;
    }

    public void setWireframeOnly(Boolean wireframeOnly) {
        this.wireframeOnly = wireframeOnly;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }

    public void cameraForward(double s) {
        camera = camera.forward(s);
    }

    public void cameraBackward(double s) {
        camera = camera.backward(s);
    }

    public void cameraUp(double s) {
        camera = camera.up(s);
    }

    public void cameraDown(double s) {
        camera = camera.down(s);
    }

    public void cameraLeft(double s) {
        camera = camera.left(s);
    }

    public void cameraRight(double s) {
        camera = camera.right(s);
    }

    public Camera getCamera() {
        return camera;
    }
}
