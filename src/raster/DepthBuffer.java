package raster;

import java.util.Arrays;

public class DepthBuffer implements Raster<Double> {
    private final double[][] buffer;
    private Double clearValue;

    public DepthBuffer(ImageBuffer imageBuffer) {
        buffer = new double[imageBuffer.getWidth()][imageBuffer.getHeight()];
        setClearValue(1.);
        clear();
    }

    @Override
    public void clear() {
        for (double[] d : buffer) {
            Arrays.fill(d, clearValue);
        }
    }

    @Override
    public void setClearValue(Double value) {
        this.clearValue = value;
    }

    @Override
    public int getWidth() {
        return 0;
    }

    @Override
    public int getHeight() {
        return 0;
    }

    @Override
    public Double getElement(int x, int y) {
        return buffer[x][y];
    }

    @Override
    public void setElement(int x, int y, Double value) {
        if (checkBorders(x, y)) {
            buffer[x][y] = value;
        }
    }
}
