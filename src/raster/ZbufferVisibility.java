package raster;

import transforms.Col;

public class ZbufferVisibility {
    private ImageBuffer iBuffer;
    private DepthBuffer zBuffer;

    public ZbufferVisibility(int width, int height) {
        this(new ImageBuffer(width, height));
    }

    public ZbufferVisibility(ImageBuffer imageBuffer) {
        iBuffer = imageBuffer;
        zBuffer = new DepthBuffer(imageBuffer);
    }

    public void drawElementWithZtest(int x, int y, double z, Col color) {
        if (iBuffer.checkBorders(x, y)) {
            if (zBuffer.getElement(x, y) >= z) {
                zBuffer.setElement(x, y, z);
                iBuffer.setElement(x, y, color);
            }
        }
    }

    public void clear() {
        zBuffer.clear();
    }

    public ImageBuffer getImage() {
        return iBuffer;
    }
}
